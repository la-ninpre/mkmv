# mkmv

**mkmv** (**m**a**k**e **m**usic **v**ideo) -- is a little shell script that
generates dumb music video from audio files and cover images. also it ouputs
all the timecodes for tracks into `timecodes.txt` file. this is suitable
for youtube upload.

## prerequisites

- ffmpeg
- ImageMagick (optionally, run with `--black_bg` to not use it)

## installation

just put `mkmv` somewhere in your `$PATH`, make sure it's executable.

## usage

cd into directory with music files and cover art image, for example:

```
$ cd ~/Music/album
$ ls
01.track 1.mp3
02.track 2.mp3
03.track 3.mp3
cover.jpg
```

then just run

```
$ mkmv
```

currently mp3, ogg, wav, and flac formats are supported for sound files.
cover art should be named 'cover' and should have jpg or png extension.

## implementation details

algorithm could be described as follows:

1. copy original files to created `originals` directory

2. remove all of the spaces from filenames and also prepend filenames with
  track number for correct track order

3. convert all files to flac format
    
  this is made for track padding to work, because lossy compressed formats
  don't allow to get exact track length.

  since most of the tracks have weird lengths (like 143.573834 seconds)
  and if just concatenate tracks one to another without any processing
  this will lead to this fractions of seconds to add up which would result
  in wrong timecodes.

  to fix that i decided to calculate residuals between 'perfect' track
  length and actual track length and to add that amount of silence to
  the beginning of the next track (because ffmpeg don't allow another way
  around).

4. pad tracks with silence to get exact timecodes

5. pad cover image with chosen method to fit 1920x1080 resolution

  currently there are two methods: one uses ffmpeg to place the image
  centered on a black background with a canvas size of 1920x1080, and
  another uses imagemagick and does similar thing but background is
  the cover itself, but scaled up and blurred, which looks slightly
  better.

6. concatenate tracks together

7. make a compound movie clip with concatenated tracks and padded cover art

## caveats

script totally freaks out if invoked in directory without audiofiles
and/or cover image (need to write file check).

as for now it assumes that:

- your cover art image is named [Cc]over.(jpe?g|png) and located
  in the same directory

- your music files have at least these metadata tags filled in:
  - artist
  - title
  - album
  - track number

for other cases it may not yet work correctly.

## license

ISC (see `LICENCE` file).
